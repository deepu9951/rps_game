import collections
import random



# game_check is a function to check with the rules of the game
def game_check(u_choice,c_choice):

    possibilities = {'paper': {'rock': 'paper', 'scissors': 'scissors'},
                     'rock': {'paper': 'paper', 'scissors': 'rock'},
                     'scissors': {'rock': 'rock', 'paper': 'scissors'}}

    if u_choice != c_choice:
        res = possibilities[u_choice][c_choice]
        print('{0} won'.format(res))
        return res

    elif u_choice == c_choice:  #Game will be Tied when the user choice is same as the c_choice and hence returns No Result
        print('Game Tied')
        res = 'NR'
        return res

#init_dict is a function to initialize the dictionary

def init_dict(j, user_ch, comp_ch, flag):
    local_dictionary = collections.defaultdict(list)
    for j in range(0,10):
        local_dictionary[j].append(user_ch)
        local_dictionary[j].append(comp_ch)
        local_dictionary[j].append(flag)
    return local_dictionary

#show_history is a function to show the history of the game

def show_history(show_dict):
    round = int(input("Enter the round for which you need the information >> "))
    print("Player Choice - {0}".format(show_dict[round - 1][0]))
    print("Computer Choice - {0}".format(show_dict[round - 1][1]))
    if show_dict[round - 1][2] == 1:
        print("Player Won Round {0} ".format(round))
    elif show_dict[round - 1][2] == 0:
        print("Computer Won Round {0} ".format(round))
    elif show_dict[round - 1][2] == 2:
        print("Match Tied on Round {0} ".format(round))
    show_again = input("Whether you want to check the history once again? (y/Y)")
    show_again_ch = show_again.lower()
    if (show_again_ch == 'y'):
        show_history(show_dict)




#Starting of the program
ch = 'y'
while(ch == 'y'):
    user_points=0
    comp_points=0
    name = input("Enter your Name: ")
    print("Welcome {0}".format(name))
    print("Starting a Game of Rock Paper Scissor between you and Computer")

  #Game Input
    for i in range(1,11):
        print("Choices Available \n rock \n paper \n scissor")
        choice_tuple = ('rock', 'paper', 'scissor')

        while True:
            usi_choice = input("Enter your choice:")
            user_choice = usi_choice.lower()
            if user_choice in choice_tuple:
                print("Your Choice is {0}".format(user_choice))
                break
            else:
                print("Enter a Valid Choice")

        comp_choice = random.choice(choice_tuple)
        print("Computer's Choice is {0} ".format(comp_choice))

        result = game_check(user_choice, comp_choice)

#Scorecard Calculation

        if result == user_choice:
            k = 1
            user_points += 1
            if (comp_points < user_points):
                print("You are leading with {0} points".format(user_points))
            elif (comp_points > user_points):
                print("Computer is leading with {0} points".format(comp_points))
            elif (comp_points == user_points):
                print("Scores Level with {0} points".format(comp_points))

        elif result == comp_choice:
            k = 0
            comp_points += 1
            if (comp_points < user_points):
                print("You are leading with {0} points".format(user_points))
            elif (comp_points > user_points):
                print("Computer is leading with {0} points".format(comp_points))
            elif (comp_points == user_points):
                print("Scores Level with {0} points".format(comp_points))

        elif result == 'NR':
            k = 2
            if(comp_points < user_points):
                print("You are leading with {0} points".format(user_points))
            elif(comp_points > user_points):
                print("Computer is leading with {0} points".format(comp_points))
            elif (comp_points == user_points):
                print("Scores Level with {0} points".format(comp_points))

        central_dictionary = init_dict(i, user_choice, comp_choice, k)

#Final Result of the Game

    if(user_points > comp_points):
        print("Congratulations!!! You have won the Game")
        print("Final Score \n {0} - {1} points \n Computer - {2} points".format(name, user_points, comp_points))
    elif(user_points < comp_points):
        print("Sorry!!! You have lost the Game")
        print("Final Score \n Computer - {0} points \n {1} - {2} points".format(comp_points, name, user_points))
    elif(user_points == comp_points):
        print("Game Tied")
        print("Final Score \n Computer - {0} points \n {1} - {2} points".format(comp_points, name, user_points))

    user_response = input("Would you like to continue the restart the game? (y/Y)")
    ch = user_response.lower()

user_response_history = input("Would you like to show the History of the game? (y/Y)")
history_ch = user_response_history.lower()
if history_ch == 'y':
    show_history(central_dictionary)




